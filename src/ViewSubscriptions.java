
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import java.awt.GridLayout;

import javax.swing.AbstractListModel;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.FlowLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import java.awt.Font;

public class ViewSubscriptions extends JFrame {

	private JPanel contentPane;
	public static String userName; 
	public static ArrayList<String> list; 

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewSubscriptions frame = new ViewSubscriptions();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws FileNotFoundException 
	 */
	public ViewSubscriptions() throws FileNotFoundException {
		this.userName = Dashboard.username; 
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		
		// SET UP 
		JList <String> myList = new JList<String>();
		list = new ArrayList<String>();
		DataModel  model = new DataModel(list);
		myList.setModel(model);
					

		JScrollPane scrollPane = new JScrollPane(myList);
		panel.add(scrollPane);
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		panel_1.setLayout(new GridLayout(1, 0, 0, 0));
		
		// BACK BUTTON
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)
			{
				// GO BACK TO DASHBOARD
				Dashboard dashboard = new Dashboard(); 
				dashboard.setVisible(true);
				dispose(); 
			}
		});
		
		btnBack.setFont(new Font("Tahoma", Font.PLAIN, 12));
		panel_1.add(btnBack);
		
		// ADD SUBSCRIPTION BUTTON
		JButton btnAddSubscription = new JButton("Add Subscription");
		btnAddSubscription.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				// POP UP A TEXT BOX FOR THE USER TO ENTER A NEW URL
				String newSubscription = JOptionPane.showInputDialog("Enter a URL:"); 
				
				if(!(newSubscription.equals(null)))
				{
					model.add(newSubscription);	
					try 
					{
						writeToFile(newSubscription);
					} 
					
					catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		btnAddSubscription.setFont(new Font("Tahoma", Font.PLAIN, 12));
		panel_1.add(btnAddSubscription);
		
		// DELETE SUBSCRIPTION BUTTON
		JButton btnNewButton = new JButton("Delete Subscription");
		btnNewButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				int index = myList.getSelectedIndex(); 
				String value = myList.getSelectedValue(); 
				
				if(index != -1)
				{
					model.remove(index);
				}
				
				try {
					deleteFromFile(value);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
		panel_1.add(btnNewButton);

	}
	
	/**
	 * Writes the new subscription to the file. 
	 * @param string
	 * @throws IOException
	 */
	private void writeToFile(String string) throws IOException
	{
		FileWriter fileWriter = new FileWriter(userName + ".txt", true);
		PrintWriter writer = new PrintWriter(fileWriter);
		
		writer.append(string + "\n");
		
		// Flush and close the writer. 
		writer.flush(); 
		writer.close(); 
	}
	
	private void deleteFromFile(String string) throws IOException
	{
		FileWriter writer = new FileWriter(userName + ".txt");
		PrintWriter print = new PrintWriter(writer); 
		
		// Rewrite the file. If the string is the one that we are deleting, skip over it. 
		for (int i = 0; i < list.size(); i++) 
		{
			if(!(string.equals(list.get(i))))
			{
				System.out.println(list.get(i));
				print.write(list.get(i) + "\n");
			}
			
			print.flush(); 
		}
	}
	
}

class DataModel extends AbstractListModel<String> {
	private static final long serialVersionUID = 1L;
	
	ArrayList<String> data;
	
	File file;
	
	public DataModel(ArrayList <String> data) throws FileNotFoundException
	{
		this.data = data;

		file = new File(ViewSubscriptions.userName + ".txt"); 
		Scanner scan = new Scanner(file); 
		
		while(scan.hasNextLine())
		{
			String line = scan.nextLine(); 
			add(line);
		}
		
		scan.close();
	}
	
	@Override
	/**
	 * Gets the size of the data list. 
	 */
	public int getSize() 
	{
		return data.size();
	}

	@Override
	/**
	 * Returns an the value at a given index. 
	 */
	public String getElementAt(int index) 
	{
		return data.get(index);
	}
	
	/**
	 * Adds an element to the data list. 
	 * @param s
	 */
	public void add(String subscription) 
	{
		data.add(subscription);
		this.fireIntervalAdded(this,data.size()-1, data.size()-1);
	}
	
	public void remove(int index) 
	{
		data.remove(index);
		this.fireIntervalRemoved(this, data.size()-1, data.size()-1);
	}
}

