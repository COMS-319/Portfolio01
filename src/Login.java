
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;
import java.awt.event.ActionEvent;
import java.awt.FlowLayout;
import java.awt.CardLayout;
import javax.swing.BoxLayout;
import javax.swing.SpringLayout;

public class Login extends JFrame {

	private JPanel contentPane;
	private JPanel panel;
	private JPanel panel_1;
	private JLabel lblUsername;
	public static JTextField txtUsername;
	private JLabel lblPassword;
	private JPasswordField pwdPassword;
	private JPanel panel_2;
	private JButton btnLogin;
	private JButton btnCancel;
	private JPanel panel_3;
	private JButton btnRegister;
	
	private DataOutputStream out; 
	private DataInputStream in; 

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(4, 0, 2, 0));
		
		panel = new JPanel();
		contentPane.add(panel);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		lblUsername = new JLabel("Username:");
		lblUsername.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblUsername);
		
		txtUsername = new JTextField();
		txtUsername.setText("");
		panel.add(txtUsername);
		txtUsername.setColumns(10);
		
		panel_1 = new JPanel();
		contentPane.add(panel_1);
		panel_1.setLayout(new GridLayout(0, 2, 0, 0));
		
		lblPassword = new JLabel("Password: ");
		lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
		panel_1.add(lblPassword);
		
		pwdPassword = new JPasswordField();
		pwdPassword.setText("");
		panel_1.add(pwdPassword);
		
		panel_2 = new JPanel();
		contentPane.add(panel_2);
		
		btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String userName = txtUsername.getText(); 
				String password = pwdPassword.getText(); // Using depreciated method because it makes it easier. 
				
				// AUTHENTICATE
				int authenticate = authenticate(userName, password); 
				
				// IF THE USER CAN ACCESS THE SERVER, GO TO THE NEXT PAGE. 
				if(authenticate == 0)
				{
					Dashboard dashboard = new Dashboard(); 
					dashboard.setVisible(true);
					dispose(); 
				}
				
				//IF THEY CANNOT, SHOW A MESSAGE, THEN RESET THE PAGE. 
				else if(authenticate == 1) // User's password was incorrect. 
				{
					 JOptionPane.showMessageDialog(null, "Password was incorrect");
					 txtUsername.setText("");
					 pwdPassword.setText("");
				}
				
				else // User's username could not be found. 
				{
					 JOptionPane.showMessageDialog(null, "Could not find " + userName + " in our records");
					 txtUsername.setText("");
					 pwdPassword.setText("");
				}
				
			}
		});
		panel_2.setLayout(new GridLayout(0, 2, 0, 0));
		panel_2.add(btnLogin);
		
		// CANCEL BUTTON. 
		btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener()
		{
			// IF THE USER CLICKS CANCEL, CLOSE THE PROGRAM.  
			public void actionPerformed(ActionEvent arg0) 
			{
				dispose(); 
			}
			
		});
		panel_2.add(btnCancel);
		
		panel_3 = new JPanel();
		contentPane.add(panel_3);
		
		btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				Register register = new Register(); 
				register.setVisible(true);
				setVisible(false); 
				
			}
		});
		panel_3.add(btnRegister);
	}
	
	/**
	 * Looks at the user database to determine if the username can be authenticated or not. 
	 * @param username
	 * @param password
	 * @return 0 if the user could be authenticated
	 * 		   1 if the users password was incorrect. 
	 * 		   2 if the username could not be found. 
	 */
	private int authenticate(String username, String password)
	{
		String line = null; 
		String autheticate = username + " " + password; 
		FileReader fileReader = null;
		
		try // Try and open the file for reading. 
		{
			fileReader = new FileReader("users.txt");
		} 
		
		catch (FileNotFoundException e1)
		{
			e1.printStackTrace();
		} 
		
		BufferedReader reader = new BufferedReader(fileReader); 
		
		// Loop through the users database.
		try 
		{
			while((line = reader.readLine()) != null)
			{
				// If we match, we're good to go. 
				if(line.equals(autheticate))
				{
					return 0; 
				}
				
				else if(line.startsWith(username))
				{
					return 1; 
				}
				
				else
				{
					continue; 
				}
			}
		} 
			
		catch (IOException e) 
		{
			e.printStackTrace();
		}

		return 2;		
	}
}
