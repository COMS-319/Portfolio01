
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class WebCrawler extends Thread {

	public WebCrawler() throws IOException {
//		File dir = new File(".");
//		String loc = dir.getCanonicalPath() + File.separator + "\\" + Dashboard.username + "Articles.txt";
//		FileWriter fstream = new FileWriter(loc, true);
//		BufferedWriter out = new BufferedWriter(fstream);
//		out.newLine();
//		out.close();
		start();	//threading 

	}

	public static void main(String[] args) throws IOException {

	}

	// givn a String, and a File
	// return if the String is contained in the File
	public static boolean checkExist(String s, File fin) throws IOException {

		FileInputStream fis = new FileInputStream(fin);
		// //Construct the BufferedReader object
		BufferedReader in = new BufferedReader(new InputStreamReader(fis));

		String aLine = null;
		while ((aLine = in.readLine()) != null) {
			// //Process each line
			if (aLine.trim().contains(s)) {
				in.close();
				fis.close();
				return true;
			}
		}

		// do not forget to close the buffer reader
		in.close();
		fis.close();

		return false;
	}

	public void run() {

		File file = new File(Dashboard.username + ".txt");
		Scanner s;
		try {
			s = new Scanner(file);
			while (s.hasNextLine()) {

				String URL = s.nextLine();
				String result[] = URL.split("\\.", -2); //reads every url from <user>.txt.
				processPage(URL, result[1]);
			}
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String scraper(String URL) throws IOException {
		Document doc = Jsoup.connect(URL).get();
		Elements ps = doc.select("p");		//the text lines in html start with <p>
		String ret = addLinebreaks(ps.text(), 200); // makes sure every story isn't just one long line
		return ret;
	}

	public static String addLinebreaks(String input, int maxLineLength) {
		StringTokenizer tok = new StringTokenizer(input, " ");
		StringBuilder output = new StringBuilder(input.length());
		int lineLen = 0;
		while (tok.hasMoreTokens()) {
			String word = (tok.nextToken() + " ");

			if (lineLen + word.length() > maxLineLength) {
				output.append("\n");
				lineLen = 0;
			}
			output.append(word);
			lineLen += word.length();
		}
		return output.toString();
	}

	public static void processPage(String URL, String host) throws IOException {

		File dir = new File(".");
		String loc = dir.getCanonicalPath() + "\\" + Dashboard.username + "Articles.txt";

		File file = new File(loc);
		Document doc = Jsoup.connect(URL).get();	//connects to the site

		Elements links = doc.select("a[href]");
		for (Element link : links) {	//looks at each link in doc
			String current = link.attr("href");
			if (current.charAt(0) == '/') {	//sometimes the link.attr("href) doesn't contain the domain name
				current = URL + current;
			}
			boolean e = checkExist(current, file);
			if (!e && (current.contains("story") || (current.contains("news")) || (current.contains("opinion"))
					|| (current.contains("entry")) || (current.contains("articles")))) {	// a few keywords that are in the urls of various articles
				String story = scraper(current);	//uses the current link to look into the url and copy text
				FileWriter fstream = new FileWriter(loc, true);
				BufferedWriter out = new BufferedWriter(fstream);
				out.newLine();
				out.write("link : " + current);
				out.newLine();
				out.write("text : " + link.text() + story);
				out.newLine();
				out.close();

			}
		}

	}
}
