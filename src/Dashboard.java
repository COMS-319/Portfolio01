
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import java.awt.Font;

public class Dashboard extends JFrame {

	private JPanel contentPane;
	
	public static String username; 

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dashboard frame = new Dashboard();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Dashboard() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		JPanel panel = new JPanel();
		contentPane.add(panel);
		
		username = Login.txtUsername.getText(); 
		
		JLabel lblNewLabel = new JLabel("Welcome, " + username);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 33));
		panel.add(lblNewLabel);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1);
		
		// VIEW SUBSCRIPTIONS 
		JButton btnViewSubscriptions = new JButton("View Subscriptions");
		btnViewSubscriptions.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				ViewSubscriptions view = null;
				
				try 
				{
					view = new ViewSubscriptions();
				}
				
				catch (FileNotFoundException e1) 
				{
					e1.printStackTrace();
				} 
				
				view.setVisible(true);
				dispose(); 
			}
		});
		panel_1.setLayout(new GridLayout(0, 2, 0, 0));
		panel_1.add(btnViewSubscriptions);
		
		JButton btnViewArticles = new JButton("View Articles");
		btnViewArticles.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e)
			{
				ViewArticles view = null;
				try {
					view = new ViewArticles();
				} catch (IOException | InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				view.setVisible(true);
				dispose();
			}
		});
		panel_1.add(btnViewArticles);
	}

}
