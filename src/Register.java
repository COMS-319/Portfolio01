
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class Register extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsername;
	private JPasswordField pwdPassword;
	private JPasswordField pwdConfirmPassword;
	private File users; 

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Register frame = new Register();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Register() {
				
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(4, 0, 2, 0));
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		panel_1.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setHorizontalAlignment(SwingConstants.CENTER);
		panel_1.add(lblUsername);
		
		txtUsername = new JTextField();
		panel_1.add(txtUsername);
		txtUsername.setColumns(10);
		
		JPanel panel_2 = new JPanel();
		panel.add(panel_2);
		panel_2.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
		panel_2.add(lblPassword);
		
		pwdPassword = new JPasswordField();
		panel_2.add(pwdPassword);
		
		JPanel panel_3 = new JPanel();
		panel.add(panel_3);
		panel_3.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lblConfirmPassword = new JLabel("Confirm Password:");
		lblConfirmPassword.setHorizontalAlignment(SwingConstants.CENTER);
		panel_3.add(lblConfirmPassword);
		
		pwdConfirmPassword = new JPasswordField();
		panel_3.add(pwdConfirmPassword);
		
		JPanel panel_4 = new JPanel();
		panel.add(panel_4);
		panel_4.setLayout(new GridLayout(1, 0, 0, 0));
		
		JButton btnRegister = new JButton("Register");
		btnRegister.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				String username; 
				String password; 
				String confirmPassword; 

				 username = txtUsername.getText(); 
				 password = pwdPassword.getText(); // Using depreciated methods because it is much easier :)
				 confirmPassword = pwdConfirmPassword.getText();
				 					
				 // IF THERE IS AN ERROR IN THE PASSWORD OR USERNAME, RESET THE SCREEN. 
				 if(validateUserName(username) == false || validatePassword(password, confirmPassword) == false)
				 {
					 reset(); 
				 }
				 
				 // IF BOTH CONDITIONS PASS, BREAK
				 else 
				 {					 	 
					// ADD THEM TO THE LIST OF USERS. 
				 	writerToUserLog(username, password); 
				 	
				 	// MAKE A NEW FILE FOR THEM. 
				 	createNewUserFile(username);

					
					// NAVIGATE BACK TO THE LOGIN SCREEN. 
					Login login = new Login(); 
					login.setVisible(true); 
					setVisible(false);
				 }
			}
		});
		panel_4.add(btnRegister);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				reset(); 
				
				// NAVIGATE BACK TO THE LOGIN SCREEN
				Login login = new Login(); 
				login.setVisible(true);
				setVisible(false); 
			}
		});
		panel_4.add(btnCancel);
	}
	
	/**
	 * Makes a new user file. 
	 * @param username
	 * 	user's username
	 */
	private void createNewUserFile(String username)
	{
		 // MAKE A NEW USER FILE. 
		 String fileName = username + ".txt"; 
		 File file = new File(fileName); 
		 try 
		 {
			file.createNewFile();
		 }
		 
		 catch (IOException e2) 
		 {
			e2.printStackTrace();
		 } 
	}

	/**
	 * Adds their information to the 'database' of users. 
	 * @param username
	 * 	user's username
	 * @param password
	 * 	user's password
	 */
	private void writerToUserLog(String username, String password)
	{
		File user = new File("users.txt"); 
		
		// If the file does not exist, make a new one. 
		if(!user.exists())
		{
			try 
			{
				user.createNewFile();
			} 
			
			catch (IOException e)
			{
				e.printStackTrace();
			} 
		}
		
		BufferedWriter out = null;
		try 
		{
			out = new BufferedWriter(new FileWriter(user, true));
			out.newLine();
		} 
		
		catch (IOException e1) 
		{
			e1.printStackTrace();
		} 
		try 
		{
			out.write(username + " " + password + "\n");
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		try 
		{
			out.close();
		} 
		
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
	}
	
	/**
	 * Resets the text and password fields. 
	 */
	private void reset()
	{
		 txtUsername.setText("");
		 pwdPassword.setText("");
		 pwdConfirmPassword.setText("");
	}
	
	/**
	 * Valiates the password
	 * @param password
	 * 	password to validate
	 * @param confirmPassword
	 * 	password to check if they are the same
	 * @return
	 * 	true for a valid password
	 * 	false for a invalid password
	 */
	private boolean validatePassword(String password, String confirmPassword)
	{
		// PASSWORD FIELD CANNOT BE LEFT ENTRY
		if(password.isEmpty())
		{
			 JOptionPane.showMessageDialog(null, "Password cannot be left empty.");
			 return false; 
		}
		
		// CONFIRM PASSWORD FIELD CANNOT BE LEFT EMPTY. 
		else if(confirmPassword.isEmpty())
		{
			 JOptionPane.showMessageDialog(null, "Confrim Password cannot be left empty");
			 return false; 

		}
		
		// PASSWORDS CANNOT CONTAIN A SPACE. 
		else if (password.contains(" "))
		{
			JOptionPane.showMessageDialog(null, "Username cannot contain a space.");
			reset(); 
			return false; 
		}
		
		 // PASSWORD AND CONFIRM PASSWORD MUST MATCH. 
		 else if(!(password.equals(confirmPassword)))
		 {
			 JOptionPane.showMessageDialog(null, "Your passwords didn't match, try again.");
			 return false; 
		 }
		
		// GOOD TO GO!
		 else
		 {
			return true; 
		 }
		
	}
	
	/**
	 * Validates the user name
	 * @return True for a valid username, false for a non valid. 
	 */
	private boolean validateUserName(String username)
	{
		 // USERNAME MUST HAVE A VALUE. 
		 if(username.isEmpty())
		 {
			 JOptionPane.showMessageDialog(null, "Username cannot be left empty.");
			 return false; 
		 }
		 
		 // USERNAME CANNOT HAVE A SPACE 
		 else if(username.contains(" "))
		 {
			 JOptionPane.showMessageDialog(null, "Username cannot contain a space.");			 
			 return false; 
		 }
		 
		 // IF THE USERNAME ALREADY EXISTS, TRY AGAIN. 
		 else if(checkUserName(username) == true)
		 {
			 JOptionPane.showMessageDialog(null, "Username already in the database.");
			 
			 return false; 
		 }
		 
		 else
		 {
			return true;
		 }
	}
	
	
	/**
	 * Check if the username is already made. 
	 * @throws IOException 
	 * 
	 * returns true if there is a password already in the database. 
	 */
	private boolean checkUserName(String userName)
	{
		File file = new File("users.txt"); 

		// THERE IS NO DATABASE YET, SO WE ARE GOOD TO GO. 
		if(!file.exists())
		{
			return false; 
		}
		String line = null; 
		FileReader fileReader = null;
		try 
		{			
			fileReader = new FileReader("users.txt");
		} 
		
		catch (FileNotFoundException e1)
		{
			e1.printStackTrace();
		} 
		
		BufferedReader reader = new BufferedReader(fileReader); 
		
		try 
		{
			// Loop through the file
			while((line = reader.readLine()) != null)
			{
				// If the line starts with the username, we know that the user name already exists. 
				if(line.startsWith(userName))
				{
					return true; 
				}
			}
		}
		
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		return false;
	}
}
